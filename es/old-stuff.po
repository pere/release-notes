# old-stuff.po translation to Spanish
# Copyright (C) 2009-2019 Free Software Foundation, Inc.
# This file is distributed under the same license as the release-notes.
#
# Copyright (C) Translators and reviewers (see below) of the Debian Spanish translation team
# - Translators:
#       Ricardo Cárdenes Medina
#       Juan Manuel García Molina
#       Javier Fernández-Sanguino Peña
#       David Martínez Moreno
#       Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2009
# - Reviewers:
#        Martín Ferrari, 2007
#
# Changes:
#   - Updated for Lenny
#       Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2009
#   - Updated for Squeeze
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2010-2011
#   - Updated for Wheezy
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2013
#   - Updated for Jessie
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2015
#   - Updated for Stretch
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2017
#   - Updated for Buster
#       Javier Fernández-Sanguino Peña <jfs@debian.org>, 2019
#
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
#       info -n '(gettext)PO Files'
#       info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
#   - El proyecto de traducción de Debian al español
#     http://www.debian.org/intl/spanish/
#     especialmente las notas de traducción en
#     http://www.debian.org/intl/spanish/notas
#
#   - La guía de traducción de po's de debconf:
#     /usr/share/doc/po-debconf/README-trans
#     o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2019-07-12 00:10+0200\n"
"Last-Translator: Javier Fernández-Sanguino <jfs@debian.org>\n"
"Language-Team: Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: ftp http get list update apt sources unstable stable\n"
"X-POFile-SpellExtra: cdrom deb root UTF vd Debian dpkg\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "es"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Gestión de su sistema &oldreleasename; antes de la actualización"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Este apéndice contiene la información sobre cómo asegurarse de que puede "
"instalar o actualizar los paquetes de &oldreleasename; antes de actualizar a "
"&releasename;. Esto solo debería ser necesario en situaciones muy concretas."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Actualizar su sistema &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Esta tarea es básicamente como cualquier otra actualización de "
"&oldreleasename; que haya realizado. La única diferencia es que primero "
"necesita asegurarse de que su lista de paquetes contiene referencias a "
"&oldreleasename; tal y como se describe en <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Si actualiza su sistema usando una réplica de Debian, automáticamente se "
"actualizará a la última versión de &oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your APT source-list files"
msgstr "Comprobar su lista de fuentes APT"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Si existe alguna referencia en sus archivos de fuentes APT (consulte <ulink "
"url=\"&url-man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</"
"ulink>) contienen referencias a <quote><literal>stable</literal></quote>, ya "
"está utilizando &releasename;. Esto puede no ser lo que Vd. desee si no está "
"preparado aún para hacer la actualización. Si ya ha ejecutado <command>apt "
"update</command>, todavía puede volver a atrás sin problemas siguiendo el "
"procedimiento explicado a continuación."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Si también ha instalado los paquetes desde &releasename;, probablemente ya "
"no tiene mucho sentido instalar paquetes desde &oldreleasename;. En ese "
"caso, tendrá que decidir si quiere continuar o no. Es posible instalar una "
"versión anterior de un paquete, pero ese procedimiento no se describe aquí."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Abra el archivo (como <literal>root</literal>) las fuentes apropiadas de APT "
"(como <filename>/etc/apt/sources.list</filename>) con su editor favorito y "
"compruebe todas las las líneas que comiencen por <literal>deb http:</"
"literal>, <literal>deb https:</literal>, <literal>deb tor+http:</literal>, "
"<literal>deb tor+https:</literal>, <literal>URIs: http:</literal>, "
"<literal>URIs: https:</literal>, <literal>URIs: tor+http:</literal> o "
"<literal>URIs: tor+https:</literal> para ver si existe alguna referencia a "
"<quote><literal>stable</literal></quote>. Si encuentra alguna, cambie "
"<literal>stable</literal> por <literal>&oldreleasename;</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Si existe alguna línea que comienza por <literal>deb file:</literal> o "
"<literal>URIs: file:</literal>, tendrá que comprobar si la ubicación a la "
"que hace referencia contiene un archivo de &oldreleasename; o de "
"&releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"No cambie ninguna línea que comience por <literal>deb cdrom:</literal> o "
"<literal>URIs: cdrom:</literal>. Hacerlo invalidaría la línea y tendría que "
"ejecutar de nuevo <command>apt-cdrom</command>. No se preocupe si alguna "
"línea de una fuente de <literal>cdrom</literal> hace referencia a "
"<quote><literal>unstable</literal></quote>. Puede parecer confuso, pero es "
"normal."

# Cam: supongo que los dos puntos son intencionados.
#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Si ha realizado algún cambio, guarde el archivo y ejecute:"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr "# apt update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "para actualizar la lista de paquetes."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "Borrar ficheros de configuración obsoletos"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Antes de actualizar su sistema a &releasename; es recomendable borrar los "
"ficheros de configuración obsoletos (como los archivos <filename>*.dpkg-{new,"
"old}</filename> que se puedan encontrar bajo el directorio <filename>/etc</"
"filename> del sistema."

#~ msgid "Upgrade legacy locales to UTF-8"
#~ msgstr "Actualización de localizaciones antiguas a UTF-8"

#~ msgid ""
#~ "Using a legacy non-UTF-8 locale has been unsupported by desktops and "
#~ "other mainstream software projects for a long time. Such locales should "
#~ "be upgraded by running <command>dpkg-reconfigure locales</command> and "
#~ "selecting a UTF-8 default. You should also ensure that users are not "
#~ "overriding the default to use a legacy locale in their environment."
#~ msgstr ""
#~ "El uso de las localizaciones antiguas no-UTF-8 han dejado de estar "
#~ "soportadas hace mucho tiempo tanto por los entornos de escritorio como "
#~ "por otros projectos importantes. Estas localizaciones deberían "
#~ "actualizarse ejecutando <command>dpkg-reconfigure locales</command> y "
#~ "seleccionando una localización UTF-8 como valor por omisión. Debería "
#~ "también asegurarse que los usuarios no cambian el valor por omisión "
#~ "utilizando una localización obsoleta en su entorno."

#~ msgid ""
#~ "<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
#~ "\">Debian will remove FTP access to all of its official mirrors on "
#~ "2017-11-01</ulink>.  If your sources.list contains a <literal>debian.org</"
#~ "literal> host, please consider switching to <ulink url=\"https://deb."
#~ "debian.org\">deb.debian.org</ulink>.  This note only applies to mirrors "
#~ "hosted by Debian itself.  If you use a secondary mirror or a third-party "
#~ "repository, then they may still support FTP access after that date.  "
#~ "Please consult with the operators of these if you are in doubt."
#~ msgstr ""
#~ "<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
#~ "\">Debian eliminará el acceso mediante FTP de todas las réplicas "
#~ "oficiales el 2017-11-01</ulink>.  Si su archivo sources.list contiene una "
#~ "servidor bajo <literal>debian.org</literal>, valore la opción de "
#~ "cambiarlo a <ulink url=\"https://deb.debian.org\">deb.debian.org</"
#~ "ulink>.  Esta nota solo aplica a réplicas hospedadas por Debian. Si "
#~ "utiliza una réplica secundaria o un repositorio de terceros, puede que "
#~ "tenga aún acceso FTP pasada esta fecha. Por favor, contacte con los "
#~ "operadores de estos servidores si tiene alguna duda."

#~ msgid ""
#~ "Lines in sources.list starting with <quote>deb ftp:</quote> and pointing "
#~ "to debian.org addresses should be changed into <quote>deb http:</quote> "
#~ "lines.  See <xref linkend=\"deprecation-of-ftp-apt-mirrors\" />."
#~ msgstr ""
#~ "Las líneas en el fichero <filename>sources.list</filename> que empiecen "
#~ "por <quote>deb ftp:</quote> y que apunten a una dirección debian.org "
#~ "deberían modificarse a líneas que empiecen por <quote>deb http:</quote>. "
#~ "Consulte <xref linkend=\"deprecation-of-ftp-apt-mirrors\" />."

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "En el salvapantallas de GNOME la utilización de contraseñas con "
#~ "caracteres no-ASCII, con soporte de pam_ldap, e incluso la capacidad de "
#~ "desbloquear la pantalla es imprevisible cuando no se utiliza la "
#~ "codificación UTF-8. El salvapantallas de GNOME está afectado por la "
#~ "errata <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>. El "
#~ "gestor de ficheros Nautilius (y todos los programas basados en glib, así "
#~ "como probablemente todos los programas basados en Qt) asumen que los "
#~ "nombres de ficheros están en UTF-8, pero el interfaz de línea de órdenes "
#~ "supone que están en la codificación de la localización actual. En el uso "
#~ "diario, los nombres de ficheros con caracteres no ASCCI no son usables en "
#~ "estos entornos. Además, el lector de pantalla gnome-orca (que permite "
#~ "acceder a las personas con deficiencia visual al entorno de escritorio de "
#~ "GNOME a las personas) a partir de Squeeze solo funciona si la "
#~ "localización es UTF-8, si se está utilizando una codificación que no sea "
#~ "UTF-8, no será capaz de leer la información de ventanas para los "
#~ "elementos de escritorio como Nautilus, el panel de GNOME o el menú Alt+F1."

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "Si su sistema está localizado y está utilizando una localización que no "
#~ "está basada en UTF-8, debería considerar seriamente el cambio a "
#~ "localizaciones basadas en UTF-8. En el pasado se han detectado "
#~ "erratas<placeholder type=\"footnote\" id=\"0\"/> que, una vez analizadas, "
#~ "solo se producen cuando se utiliza una localización que no está basada en "
#~ "UTF-8. En el entorno de escritorio, el soporte a estas localizaciones "
#~ "antiguas se hace a través de ciertos ajustes complicados en la parte "
#~ "interna de las bibliotecas y no podemos dar soporte adecuado a los "
#~ "usuarios que aún utilicen este tipo de localizaciones."

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "Puede ejecutar la orden <command>dpkg-reconfigure locales</command> para "
#~ "configurar el entorno de localización de su sistema. Asegúrese de "
#~ "utilizar una localización UTF-8 cuando se le pregunte cuál debería ser la "
#~ "localización a utilizar por omisión en el sistema. Además, debería "
#~ "revisar la configuración de localización de sus usuarios y asegurarse que "
#~ "no tienen definiciones de localizaciones antiguas en sus archivos de "
#~ "configuración del entorno."

#~ msgid ""
#~ "Since release 2:1.7.7-12, xorg-server no longer reads the file "
#~ "XF86Config-4.  See also <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
#~ msgstr ""
#~ "A partir de la versión 2:1.7.7-12, xorg-server ya no lee el archivo "
#~ "XF86Config-4. Vea también <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
