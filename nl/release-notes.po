# Dutch translation of the Debian release notes.
# Copyright (C) 2012 The Debian Project.
# This file is distributed under the same license as the Debian release notes.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes/release-notes\n"
"POT-Creation-Date: 2021-03-19 20:44+0100\n"
"PO-Revision-Date: 2019-11-08 17:20+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. type: Attribute 'lang' of: <book>
#: en/release-notes.dbk:8
msgid "en"
msgstr "nl"

#. type: Content of: <book><title>
#: en/release-notes.dbk:9
msgid "Release Notes for &debian; &release; (&releasename;), &arch-title;"
msgstr ""
"Notities bij de release van &debian; &release; (&releasename;), &arch-title;"

#. type: Content of: <book><subtitle>
#: en/release-notes.dbk:11
msgid ""
"<ulink url=\"https://www.debian.org/doc/\">The Debian Documentation Project</"
"ulink>"
msgstr ""
"<ulink url=\"https://www.debian.org/doc/\">Het Documentatieproject van "
"Debian</ulink>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:17
msgid ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"
msgstr ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:22
msgid ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"
msgstr ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:27
msgid ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"
msgstr ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:32
msgid ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"
msgstr ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:37
msgid "<firstname></firstname> <surname></surname>"
msgstr "<firstname></firstname> <surname></surname>"

#. type: Content of: <book><bookinfo><editor><contrib>
#: en/release-notes.dbk:39
msgid "There were more people!"
msgstr "En nog meer mensen!"

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:43
msgid ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as published "
"by the Free Software Foundation."
msgstr ""
"Dit document is vrije software; u mag het verspreiden en/of wijzigen onder "
"de voorwaarden van de GNU General Public License, versie 2, zoals "
"uitgebracht door de Free Software Foundation."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:49
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""
"Dit programma wordt verspreid in de hoop dat het nuttig zal zijn, maar "
"ZONDER ENIGE GARANTIE; zelfs zonder de impliciete garantie van "
"VERKOOPBAARHEID of GESCHIKTHEID VOOR EEN SPECIFIEK DOEL. Zie de GNU General "
"Public License voor meer details."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:56
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
msgstr ""
"Samen met dit programma zou u een exemplaar van de GNU General Public "
"License ontvangen moeten hebben; is dit niet het geval, schrijf dan naar de "
"Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA "
"02110-1301 USA."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:61
msgid ""
"The license text can also be found at <ulink url=\"https://www.gnu.org/"
"licenses/gpl-2.0.html\"/> and <filename>/usr/share/common-licenses/GPL-2</"
"filename> on &debian; systems."
msgstr ""
"De tekst van deze licentie is ook beschikbaar op <ulink url=\"https://www."
"gnu.org/licenses/gpl-2.0.html\"/> en <filename>/usr/share/common-licenses/"
"GPL-2</filename> op &debian;-systemen."

#. type: Content of: <book><appendix><title>
#: en/release-notes.dbk:84
msgid "Contributors to the Release Notes"
msgstr "Mensen die een bijdrage hebben geleverd aan de notities bij de release"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:86
msgid ""
"Many people helped with the release notes, including, but not limited to"
msgstr ""
"Veel mensen hebben geholpen met de notities bij de release waaronder, maar "
"niet alleen,"

#.  alphabetical (LANG=C sort) order by firstname 
#.  the contrib will not be printed, but is a reminder for the editor;
#.          username as shown in vcs log, contribution  
#.  list of translators will only show up in translated texts, only list
#.          contributors to en/ here 
#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:94
msgid "<author> <firstname>Adam</firstname> <surname>D. Barratt</surname>"
msgstr "<author> <firstname>Adam</firstname> <surname>D. Barratt</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:97
msgid "various fixes in 2013"
msgstr "verschillende verbeteringen in 2013"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:98
msgid ""
"</author>, <author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgstr ""
"</author>, <author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:102 en/release-notes.dbk:217
msgid "previous releases"
msgstr "eerdere uitgaven"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:103
msgid ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"
msgstr ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:107
msgid "aba, previous releases: 2005 - 2007"
msgstr "aba, eerdere uitgaven: 2005 - 2007"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:108
msgid ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"
msgstr ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:112 en/release-notes.dbk:152 en/release-notes.dbk:252
#: en/release-notes.dbk:272
msgid "various contributions"
msgstr "verschillende bijdrages"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:113
msgid ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"
msgstr ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:117 en/release-notes.dbk:122
msgid "previous release"
msgstr "eerdere uitgave"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:118
msgid ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"
msgstr ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:123
msgid ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"
msgstr ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:127
msgid "description of GM965 issue"
msgstr "beschrijving van het probleem met GM965"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:128
msgid ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:132
msgid "bubulle, Lenny installation"
msgstr "bubulle, de installatie van Lenny"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:133
msgid ""
"</author>, <author> <firstname>Christoph</firstname> <surname>Berg</surname>"
msgstr ""
"</author>, <author> <firstname>Christoph</firstname> <surname>Berg</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:137
msgid "PostgreSQL-specific issues"
msgstr "PostgreSQL-specifieke aangelegenheden"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:138
msgid ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"
msgstr ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:142
msgid "Debian Live"
msgstr "Debian Live"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:143
msgid ""
"</author>, <author> <firstname>David</firstname> <surname>Prévot</surname>"
msgstr ""
"</author>, <author> <firstname>David</firstname> <surname>Prévot</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:147
msgid "taffit, Wheezy release"
msgstr "taffit, de uitgave van Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:148
msgid ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"
msgstr ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:153
msgid ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"
msgstr ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:157
msgid "backports"
msgstr ""
"backports (het geschikt maken van de nieuwste software voor de huidige "
"stabiele release)"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:158
msgid ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"
msgstr ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:162
msgid "rework X11 upgrade"
msgstr "herwerken van het opwaarderen van X11"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:163
msgid "</author>, <author> <firstname>Frans</firstname> <surname>Pop</surname>"
msgstr ""
"</author>, <author> <firstname>Frans</firstname> <surname>Pop</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:167
msgid "fjp, previous release (Etch)"
msgstr "fjp, eerdere uitgave (Etch)"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:168
msgid ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:172 en/release-notes.dbk:262
msgid "innumerable contributions"
msgstr "talloze bijdragen"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:173
msgid ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:177 en/release-notes.dbk:242
msgid "ARM port issues"
msgstr "aandachtspunten bij het geschikt maken van Debian voor ARM"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:178
msgid ""
"</author>, <author> <firstname>Hideki</firstname> <surname>Yamane</surname>"
msgstr ""
"</author>, <author> <firstname>Hideki</firstname> <surname>Yamane</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:182
msgid "henrich, contributed and contributing since 2006"
msgstr "henrich, heeft bijgedragen en draagt bij sinds 2006"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:183
msgid ""
"</author>, <author> <firstname>Holger</firstname> <surname>Wansing</surname>"
msgstr ""
"</author>, <author> <firstname>Holger</firstname> <surname>Wansing</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:187
msgid "holgerw, contributed and contributing since 2009"
msgstr "holgerw, heeft bijgedragen en draagt bij sinds 2009"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:188
msgid ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"
msgstr ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:192
msgid "jfs, Etch release, Squeeze release"
msgstr "jfs, uitgave van Etch, uitgave van Squeeze"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:193
msgid ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"
msgstr ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:197
msgid "German translation, innumerable contributions"
msgstr "Duitse vertaling, talloze bijdragen"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:198
msgid ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"
msgstr ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:202 en/release-notes.dbk:247
msgid "syslog issues"
msgstr "kwesties in verband met syslog"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:203
msgid ""
"</author>, <author> <firstname>Jonathan</firstname> <surname>Nieder</surname>"
msgstr ""
"</author>, <author> <firstname>Jonathan</firstname> <surname>Nieder</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:207
msgid "jrnieder@gmail.com, Squeeze release, Wheezy release"
msgstr "jrnieder@gmail.com, uitgave van Squeeze, uitgave van Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:208
msgid ""
"</author>, <author> <firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:212
msgid "joostvb, Wheezy release, Jessie release"
msgstr "joostvb, uitgave van Wheezy, uitgave van Jessie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:213
msgid ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"
msgstr ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:218
msgid ""
"</author>, <author> <firstname>Julien</firstname> <surname>Cristau</surname>"
msgstr ""
"</author>, <author> <firstname>Julien</firstname> <surname>Cristau</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:222
msgid "jcristau, Squeeze release, Wheezy release"
msgstr "jcristau, uitgave van Squeeze, uitgave van Wheezy"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:223
msgid ""
"</author>, <author> <firstname>Justin B</firstname> <surname>Rye</surname>"
msgstr ""
"</author>, <author> <firstname>Justin B</firstname> <surname>Rye</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:227
msgid "English fixes"
msgstr "correcties Engels"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:228
msgid ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"
msgstr ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:232
msgid "description of NFS issues"
msgstr "beschrijving van kwesties in verband met NFS"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:233
msgid "</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"
msgstr ""
"</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:237
msgid "editors motivation manager"
msgstr "motivatiemanager van de redactie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:238
msgid ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:243
msgid ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"
msgstr ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:248
msgid ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:253
msgid ""
"</author>, <author> <firstname>Niels</firstname> <surname>Thykier</surname>"
msgstr ""
"</author>, <author> <firstname>Niels</firstname> <surname>Thykier</surname> "

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:257
msgid "nthykier, Jessie release"
msgstr "nthykier, uitgave van Jessie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:258
msgid ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"
msgstr ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:263
msgid ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:267
msgid "Japanese translation (coordination), innumerable contributions"
msgstr "Japanse vertaling (coördinatie), talloze bijdragen"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:268
msgid ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"
msgstr ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:273
msgid ""
"</author>, <author> <firstname>Paul</firstname> <surname>Gevers</surname>"
msgstr ""
"</author>, <author> <firstname>Paul</firstname> <surname>Gevers</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:277
msgid "elbrus, buster release"
msgstr "elbrus, uitgave van buster"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:278
msgid ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:282
msgid "kernel version note"
msgstr "opmerking over kernelversie"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:283
msgid ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"
msgstr ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:287 en/release-notes.dbk:312
msgid "Etch release"
msgstr "uitgave van Etch"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:288
msgid ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"
msgstr ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:292 en/release-notes.dbk:297
msgid "description of d-i Braille support"
msgstr ""
"beschrijving van de Braille-ondersteuning in het installatiesysteem van "
"Debian"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:293
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"
msgstr ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:298
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"
msgstr ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:302
msgid "spaillar-guest, innumerable contributions"
msgstr "spaillar-guest, talloze bijdragen"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:303
msgid ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"
msgstr ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:307
msgid "description of Apache issues"
msgstr "beschrijving van kwesties in verband met Apache"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:308
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"
msgstr ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:313
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>McIntyre</surname>"
msgstr ""
"</author>, <author> <firstname>Steve</firstname> <surname>McIntyre</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:317
msgid "Debian CDs"
msgstr "Debian cd's"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:318
msgid ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"
msgstr ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:322 en/release-notes.dbk:331
msgid "description of \"proposed-update\""
msgstr "beschrijving van \"proposed-update\""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:323
msgid "</author>, <author> <firstname>victory</firstname>"
msgstr "</author>, <author> <firstname>victory</firstname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:326
msgid ""
"victory-guest victory.deb@gmail.com, markup fixes, contributed and "
"contributing since 2006"
msgstr ""
"victory-guest victory.deb@gmail.com, verbeteringen aan de opmaak, heeft "
"bijgedragen en draagt bij sinds 2006"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:327
msgid ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:332
msgid ""
"</author>, and <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"
msgstr ""
"</author>, en <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:337
msgid "editing Lenny release, switch to DocBook XML"
msgstr "redactie van de uitgave voor Lenny, omschakeling naar DocBook XML"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:338
msgid "</author>."
msgstr "</author>."

#
#.  translator names here, depending on language!
#.     </para>
#. <para>Translated into Klingon by:
#.     <author>
#.       <firstname>Firstname1</firstname>
#.       <surname>Surname1</surname>
#.       <contrib>Foo translation</contrib>
#.     </author>,
#.     <author>
#.       <firstname>Firstname2</firstname>
#.       <surname>Surname2</surname>
#.       <contrib>Foo translation</contrib>
#.     </author 
#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:341
msgid ""
"This document has been translated into many languages.  Many thanks to the "
"translators!"
msgstr ""
"Dit document is naar vele talen vertaald. Veel dank aan de vertalers!</"
"para><para>De volgende personen droegen bij tot de Nederlandse vertaling: "
"<author><firstname>Remco</firstname><surname>Rijnders</"
"surname><contrib>(2011)</contrib></author>, <author><firstname>Eric</"
"firstname><surname>Spreen</surname><contrib>(2011)</contrib></author>, "
"<author><firstname>Jeroen</firstname><surname>Schot</"
"surname><contrib>(2012)</contrib></author>, <author><firstname>Vincent</"
"firstname><surname>Zweije</surname><contrib>(2012)</contrib></author>, "
"<author><firstname>Frans</firstname><surname>Spiesschaert</"
"surname><contrib>(2017-2019)</contrib></author>."

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:364
msgid "ACPI"
msgstr "ACPI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:365
msgid "Advanced Configuration and Power Interface"
msgstr ""
"Advanced Configuration and Power Interface (geavanceerde configuratie- en "
"energie-interface)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:368
msgid "ALSA"
msgstr "ALSA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:369
msgid "Advanced Linux Sound Architecture"
msgstr ""
"Advanced Linux Sound Architecture (geavanceerde geluidsarchitectuur voor "
"Linux)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:372
msgid "BD"
msgstr "BD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:373
msgid "Blu-ray Disc"
msgstr "Blu-ray Disc (blu-raydisc)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:376
msgid "CD"
msgstr "cd"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:377
msgid "Compact Disc"
msgstr "Compact Disc"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:380
msgid "CD-ROM"
msgstr "cd-rom"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:381
msgid "Compact Disc Read Only Memory"
msgstr ""
"Compact Disc Read Only Memory (alleen-lezen-schijfgeheugen op compact disc)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:384
msgid "DHCP"
msgstr "DHCP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:385
msgid "Dynamic Host Configuration Protocol"
msgstr ""
"Dynamic Host Configuration Protocol (protocol voor dynamische configuratie "
"van computers)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:388
msgid "DLBD"
msgstr "DLBD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:389
msgid "Dual Layer Blu-ray Disc"
msgstr "Dual Layer Blu-ray Disc (dubbellagige blu-raydisc)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:392
msgid "DNS"
msgstr "DNS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:393
msgid "Domain Name System"
msgstr "Domain Name System (systeem voor domeinnamen)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:396
msgid "DVD"
msgstr "dvd"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:397
msgid "Digital Versatile Disc"
msgstr "Digital Versatile Disc (veelzijdige digitale disk)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:400
msgid "GIMP"
msgstr "GIMP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:401
msgid "GNU Image Manipulation Program"
msgstr "GNU Image Manipulation Program (beeldbewerkingsprogramma van GNU)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:404
msgid "GNU"
msgstr "GNU"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:405
msgid "GNU's Not Unix"
msgstr "GNU's Not Unix (GNU is Unix niet)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:408
msgid "GPG"
msgstr "GPG"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:409
msgid "GNU Privacy Guard"
msgstr "GNU Privacy Guard (privacybeschermer van GNU)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:412
msgid "LDAP"
msgstr "LDAP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:413
msgid "Lightweight Directory Access Protocol"
msgstr ""
"Lightweight Directory Access Protocol (lichtgewicht protocol voor het beheer "
"van repertoria)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:416
msgid "LSB"
msgstr "LSB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:417
msgid "Linux Standard Base"
msgstr "Linux Standard Base (Linux basisnormen)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:420
msgid "LVM"
msgstr "LVM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:421
msgid "Logical Volume Manager"
msgstr "Logical Volume Manager (logisch volumebeheer)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:424
msgid "MTA"
msgstr "MTA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:425
msgid "Mail Transport Agent"
msgstr "Mail Transport Agent (instantie voor postvervoer)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:428
msgid "NBD"
msgstr "NBD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:429
msgid "Network Block Device"
msgstr "Network Block Device (netwerkblokapparaat)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:432
msgid "NFS"
msgstr "NFS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:433
msgid "Network File System"
msgstr "Network File System (netwerkbestandssysteem)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:436
msgid "NIC"
msgstr "NIC"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:437
msgid "Network Interface Card"
msgstr "Network Interface Card (netwerkkaart)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:440
msgid "NIS"
msgstr "NIS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:441
msgid "Network Information Service"
msgstr "Network Information Service (netwerkinformatiedienst)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:444
msgid "PHP"
msgstr "PHP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:445
msgid "PHP: Hypertext Preprocessor"
msgstr "PHP: Hypertext Preprocessor (hypertekstpreprocessor)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:448
msgid "RAID"
msgstr "RAID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:449
msgid "Redundant Array of Independent Disks"
msgstr ""
"Redundant Array of Independent Disks (meervoudig geheel van onafhankelijke "
"schijven)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:452
msgid "SATA"
msgstr "SATA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:453
msgid "Serial Advanced Technology Attachment"
msgstr ""
"Serial Advanced Technology Attachment (geavanceerde seriële "
"aankoppelingstechnologie)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:456
msgid "SSL"
msgstr "SSL"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:457
msgid "Secure Sockets Layer"
msgstr "Secure Sockets Layer (beveiligde socketlaag)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:460
msgid "TLS"
msgstr "TLS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:461
msgid "Transport Layer Security"
msgstr "Transport Layer Security (beveiligde transportlaag)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:464
msgid "UEFI"
msgstr "UEFI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:465
msgid "Unified Extensible Firmware Interface"
msgstr ""
"Unified Extensible Firmware Interface (geüniformiseerde uitbreidbare "
"firmware-interface)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:468
msgid "USB"
msgstr "USB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:469
msgid "Universal Serial Bus"
msgstr "Universal Serial Bus (universele seriële bus)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:472
msgid "UUID"
msgstr "UUID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:473
msgid "Universally Unique Identifier"
msgstr "Universally Unique Identifier (universele unieke naam)"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:476
msgid "WPA"
msgstr "WPA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:477
msgid "Wi-Fi Protected Access"
msgstr "Wi-Fi Protected Access (draadloos netwerk met beveiligde toegang)"

#~ msgid "APM"
#~ msgstr "APM"

#~ msgid "Advanced Power Management"
#~ msgstr "Advanced Power Management (geavanceerd energiebeheer)"

#~ msgid "IDE"
#~ msgstr "IDE"

#~ msgid "Integrated Drive Electronics"
#~ msgstr ""
#~ "Integrated Drive Electronics (elektronica voor geïntegreerde schijven)"

#~ msgid "LILO"
#~ msgstr "LILO"

#~ msgid "LInux LOader"
#~ msgstr "LInux LOader (linuxlader)"

#~ msgid "OSS"
#~ msgstr "OSS"

#~ msgid "Open Sound System"
#~ msgstr "Open Sound System (open geluidssysteem)"

#~ msgid "RPC"
#~ msgstr "RPC"

#~ msgid "Remote Procedure Call"
#~ msgstr ""
#~ "Remote Procedure Call (aanvraag tot procedure-uitvoering op afstand)"

#~ msgid "VGA"
#~ msgstr "VGA"

#~ msgid "Video Graphics Array"
#~ msgstr "Video Graphics Array (grafische beeldweergave-matrix)"

#~ msgid "previous release (Etch)"
#~ msgstr "eerdere uitgave (Etch)"

#~ msgid "February 4th, 2011"
#~ msgstr "4 februari 2011"
