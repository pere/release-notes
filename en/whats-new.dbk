<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"https://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
<!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-whats-new" lang="en">
  <title>What's new in &debian; &release;</title>
  <para>
    The <ulink url="&url-wiki-newinrelease;">Wiki</ulink> has more information
    about this topic.
  </para>

  <!--
      Sources for architecture status:
      https://release.debian.org/buster/arch_qualify.html

Some descriptions of the ports: https://www.debian.org/ports/
  -->
  <section>
    <title>Supported architectures</title>

    <para>
      The following are the officially supported architectures for &debian;
      &release;:
    </para>
    <itemizedlist>
      <listitem>
	<para>
	  32-bit PC (<literal>i386</literal>) and 64-bit PC (<literal>amd64</literal>)
	</para>
      </listitem>
      <listitem>
	<para>
	  64-bit ARM (<literal>arm64</literal>)
	</para>
      </listitem>
      <listitem>
	<para>
	  ARM EABI (<literal>armel</literal>)
	</para>
      </listitem>
      <listitem>
	<para>
	  ARMv7 (EABI hard-float ABI, <literal>armhf</literal>)
	</para>
      </listitem>
      <listitem>
	<para>
	  little-endian MIPS (<literal>mipsel</literal>)
	</para>
      </listitem>
      <listitem>
	<para>
	  64-bit little-endian MIPS (<literal>mips64el</literal>)
	</para>
      </listitem>
      <listitem>
	<para>
	  64-bit little-endian PowerPC (<literal>ppc64el</literal>)
	</para>
      </listitem>
      <listitem>
	<para>
	  IBM System z (<literal>s390x</literal>)
	</para>
      </listitem>
    </itemizedlist>

    <variablelist arch="i386">
      <varlistentry>
	<term>Baseline bump for &arch-title; to <phrase>i686</phrase></term>
	<!-- new in bookworm -->
	<listitem>
	  <para>
            The &arch-title; support (known as the Debian architecture
            &architecture;) now requires the "long NOP" instruction. Please
            refer to <xref linkend="i386-is-i686"/> for more
            information.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>

    <para>
      You can read more about port status, and port-specific information for your
      architecture at the <ulink url="&url-ports;">Debian port
      web pages</ulink>.
    </para>
  </section>

  <section id="archive-areas">
    <title>Archive areas</title>

    <para>
      The following archive areas, mentioned in the Social Contract and in the
      Debian Policy, have been around for a long time:
    </para>
    <itemizedlist>
      <listitem>
	<para>
	  main: the Debian distribution;
	</para>
      </listitem>
      <listitem>
	<para>
	  contrib: supplemental packages intended to work with the Debian
	  distribution, but which require software outside of the distribution
	  to either build or function;
	</para>
      </listitem>
      <listitem>
	<para>
	  non-free: supplemental packages intended to work with the Debian
	  distribution that do not comply with the DFSG or have other problems
	  that make their distribution problematic.
	</para>
      </listitem>
    </itemizedlist>

    <para>
      Following the <ulink url="https://www.debian.org/vote/2022/vote_003">2022
      General Resolution about non-free firmware</ulink>, the 5th point of the
      Social Contract was extended with the following sentence:
    </para>
    <blockquote>
      <para>
	The Debian official media may include firmware that is otherwise not part
	of the Debian system to enable use of Debian with hardware that requires
	such firmware.
      </para>
    </blockquote>

    <para>
      While it's not mentioned explicitly in either the Social
      Contract or Debian Policy yet, a new archive area was
      introduced, making it possible to separate non-free firmware
      from the other non-free packages:
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  non-free-firmware
	</para>
      </listitem>
    </itemizedlist>

    <para>
      Most non-free firmware packages have been moved from <literal>non-free</literal> to
      <literal>non-free-firmware</literal> in preparation for the &debian; &release; release.
      This clean separation makes it possible to build official installation images with
      packages from <literal>main</literal> and from <literal>non-free-firmware</literal>, without <literal>contrib</literal> or
      <literal>non-free</literal>. In turn, these installation images make it possible to install
      systems with only <literal>main</literal> and <literal>non-free-firmware</literal>, without
      <literal>contrib</literal> or <literal>non-free</literal>.
    </para>

    <para>
      See <xref linkend="non-free-firmware"/> for upgrades from &oldreleasename;.
    </para>
  </section>

  <section id="newdistro">
    <title>What's new in the distribution?</title>

<!-- done for bookworm
    <programlisting condition="fixme">
      TODO: Make sure you update the numbers in the .ent file
      using the changes-release.pl script found under ../
    </programlisting>
-->

    <para>
      This new release of Debian again comes with a lot more software than
      its predecessor &oldreleasename;; the distribution includes over
      &packages-new; new packages, for a total of over &packages-total;
      packages.  Most of the software in the distribution has been updated:
      over &packages-updated; software packages (this is
      &packages-update-percent;% of all packages in &oldreleasename;).
      Also, a significant number of packages (over &packages-removed;,
      &packages-removed-percent;% of the packages in &oldreleasename;) have
      for various reasons been removed from the distribution.  You will not
      see any updates for these packages and they will be marked as
      "obsolete" in package management front-ends; see <xref
      linkend="obsolete"/>.
    </para>

    <section id="major-packages">
      <title>Desktops and well known packages</title>
      <para>
	&debian; again ships with several desktop applications and
	environments.  Among others it now includes the desktop environments
	GNOME<indexterm><primary>GNOME</primary></indexterm> 43,
	KDE Plasma<indexterm><primary>KDE</primary></indexterm> 5.27,
	LXDE<indexterm><primary>LXDE</primary></indexterm> 11,
	LXQt<indexterm><primary>LXQt</primary></indexterm> 1.2.0,
	MATE<indexterm><primary>MATE</primary></indexterm> 1.26, and
	Xfce<indexterm><primary>Xfce</primary></indexterm> 4.18.
      </para>
      <para>
	Productivity applications have also been upgraded, including the
	office suites:
      </para>
      <itemizedlist>
	<listitem>
	  <para>
	    LibreOffice<indexterm><primary>LibreOffice</primary></indexterm>
	    is upgraded to version 7.4;
	  </para>
	</listitem>
	<listitem>
	  <para>
	    GNUcash<indexterm><primary>GNUcash</primary></indexterm> is upgraded to 4.13;
	  </para>
	</listitem>
	<!-- no updates?
	<listitem>
	  <para>
	    Calligra<indexterm><primary>Calligra</primary></indexterm>
	    is upgraded to 3.2.
	  </para>
	</listitem>
	     <listitem>
	     <para>
	     GNUmeric<indexterm><primary>GNUmeric</primary></indexterm> is upgraded to 1.12;
	     </para>
	     </listitem>
	     <listitem>
	     <para>
	     Abiword<indexterm><primary>Abiword</primary></indexterm> is upgraded to 3.0.
	     </listitem>
	     </ -->
      </itemizedlist>

      <!-- JFS:
	   Might it be useful point to https://distrowatch.com/table.php?distribution=debian ?
	   This provides a more comprehensive comparison among different releases -->

      <para>
	Among many others, this release also includes the following software updates:
      </para>
      <informaltable pgwide="1">
	<tgroup cols="3">
	  <colspec align="justify"/>
	  <colspec align="justify"/>
	  <colspec align="justify"/>
	  <!-- colspec align="justify" colwidth="3*"/ -->
	  <thead>
	    <row>
	      <entry>Package</entry>
	      <entry>Version in &oldrelease; (&oldreleasename;)</entry>
	      <entry>Version in &release; (&releasename;)</entry>
	    </row>
	  </thead>
	  <tbody>
	    <row id="new-apache2">
	      <entry>Apache<indexterm><primary>Apache</primary></indexterm></entry>
	      <entry>2.4.54</entry>
	      <entry>2.4.57</entry>
	    </row>
	    <row id="new-bind9">
	      <entry>BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym> Server</entry>
	      <entry>9.16</entry>
	      <entry>9.18</entry>
	    </row>
	    <!--
		<row id="new-chromium">
		<entry>Chromium<indexterm><primary>Chromium</primary></indexterm></entry>
		<entry>53.0</entry>
		<entry>73.0</entry>
		</row>
		<row id="new-courier">
		<entry>Courier<indexterm><primary>Courier</primary></indexterm> <acronym>MTA</acronym></entry>
		<entry>0.73</entry>
		<entry>1.0</entry>
		</row>
	    -->
	    <row id="new-cryptsetup">
              <entry>Cryptsetup<indexterm><primary>Cryptsetup</primary></indexterm></entry>
	      <entry>2.3</entry>
	      <entry>2.6</entry>
	    </row>
	    <!--
		<row id="new-dia">
		<entry>Dia<indexterm><primary>Dia</primary></indexterm></entry>
		<entry>0.97.2</entry>
		<entry>0.97.3</entry>
		</row>
	    -->
	    <row id="new-dovecot">
	      <entry>Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</acronym></entry>
	      <entry>2.3.13</entry>
	      <entry>2.3.19</entry>
	    </row>
	    <row id="new-emacs">
	      <entry>Emacs</entry>
	      <entry>27.1</entry>
	      <entry>28.2</entry>
	    </row>
	    <row id="new-exim4">
	      <entry>Exim<indexterm><primary>Exim</primary></indexterm> default e-mail server</entry>
	      <entry>4.94</entry>
	      <entry>4.96</entry>
	    </row>
	    <!--
		<row id="new-firefox">
		<entry>Firefox<indexterm><primary>Firefox</primary></indexterm></entry>
		<entry>45.5 (AKA Iceweasel)</entry>
		<entry>60.7 (ESR)</entry>
		</row>
	    -->
	    <row id="new-gcc">
	      <entry><acronym>GNU</acronym> Compiler Collection as default compiler<indexterm><primary>GCC</primary></indexterm></entry>
	      <entry>10.2</entry>
	      <entry>12.2</entry>
	    </row>
	    <row id="new-gimp">
	      <entry><acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm></entry>
	      <entry>2.10.22</entry>
	      <entry>2.10.34</entry>
	    </row>
	    <row id="new-gnupg">
	      <entry>GnuPG<indexterm><primary>GnuPG</primary></indexterm></entry>
	      <entry>2.2.27</entry>
	      <entry>2.2.40</entry>
	    </row>
	    <row id="new-inkscape">
	      <entry>Inkscape<indexterm><primary>Inkscape</primary></indexterm></entry>
	      <entry>1.0.2</entry>
	      <entry>1.2.2</entry>
	    </row>
	    <row id="new-libc6">
	      <entry>the <acronym>GNU</acronym> C library</entry>
	      <entry>2.31</entry>
	      <entry>2.36</entry>
	    </row>
	    <row id="new-lighttpd">
	      <entry>lighttpd</entry>
	      <entry>1.4.59</entry>
	      <entry>1.4.69</entry>
	    </row>
	    <row id="new-linux-image">
              <entry>Linux kernel image</entry>
              <entry>5.10 series</entry>
              <entry>6.1 series</entry>
	    </row>
	    <row id="llvm-toolchain">
              <entry>LLVM/Clang toolchain</entry>
              <entry>9.0.1 and 11.0.1 (default) and 13.0.1</entry>
              <entry>13.0.1 and 14.0 (default) and 15.0.6</entry>
	    </row>
	    <row id="new-mariadb">
	      <entry>MariaDB<indexterm><primary>MariaDB</primary></indexterm></entry>
	      <entry>10.5</entry>
	      <entry>10.11</entry>
	    </row>
	    <row id="new-nginx">
	      <entry>Nginx<indexterm><primary>Nginx</primary></indexterm></entry>
	      <entry>1.18</entry>
	      <entry>1.22</entry>
	    </row>
	    <row id="new-openldap">
	      <entry>OpenLDAP</entry>
	      <entry>2.4.57</entry>
	      <entry>2.5.13</entry>
	    </row>
	    <row id="new-openjdk">
	      <entry>OpenJDK<indexterm><primary>OpenJDK</primary></indexterm></entry>
	      <entry>11</entry>
	      <entry>17</entry>
	    </row>
	    <row id="new-openssh">
	      <entry>OpenSSH<indexterm><primary>OpenSSH</primary></indexterm></entry>
	      <entry>8.4p1</entry>
	      <entry>9.2p1</entry>
	    </row>
	    <row id="new-perl">
	      <entry>Perl<indexterm><primary>Perl</primary></indexterm></entry>
	      <entry>5.32</entry>
	      <entry>5.36</entry>
	    </row>
	    <row id="new-php">
	      <entry><acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm></entry>
	      <entry>7.4</entry>
	      <entry>8.2</entry>
	    </row>
	    <row id="new-postfix">
	      <entry>Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</acronym></entry>
	      <entry>3.5</entry>
	      <entry>3.7</entry>
	    </row>
	    <row id="new-postgresql">
	      <entry>PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm></entry>
	      <entry>13</entry>
	      <entry>15</entry>
	    </row>
	    <!--
		<row id="new-python">
		<entry>Python</entry>
		<entry>2.6</entry>
		<entry>2.7</entry>
		</row>
	    -->
	    <row id="new-python3">
	      <entry>Python 3</entry>
	      <entry>3.9.2</entry>
	      <entry>3.11.2</entry>
	    </row>
	    <row id="new-rustc">
	      <entry>Rustc</entry>
	      <entry>1.48</entry>
	      <entry>1.63</entry>
	    </row>
	    <row id="new-samba">
	      <entry>Samba</entry>
	      <entry>4.13</entry>
	      <entry>4.17</entry>
	    </row>
	    <row id="new-systemd">
	      <entry>systemd</entry>
	      <entry>247</entry>
	      <entry>252</entry>
	    </row>
	    <row id="new-vim">
	      <entry>Vim</entry>
	      <entry>8.2</entry>
	      <entry>9.0</entry>
	    </row>
	  </tbody>
	</tgroup>
      </informaltable>
    </section>

    <section id="man-xx">
      <!-- bullseye to bookworm -->
      <title>More translated man pages</title>
      <para>
        Thanks to our translators, more documentation in
        <command>man</command>-page format is available in more
        languages than ever. For example, many man pages are now
        available in Czech, Danish, Greek, Finnish, Indonesian,
        Macedonian, Norwegian (Bokmål), Russian, Serbian, Swedish,
        Ukrainian and Vietnamese, and all <systemitem
        role="package">systemd</systemitem> man pages are now
        available in German.
      </para>
      <para>
	To ensure the <command>man</command> command shows the
	documentation in your language (where possible), install the
	right <systemitem
	role="package">manpages-<replaceable>lang</replaceable></systemitem>
	package and make sure your locale is correctly configured by
	using
	<programlisting>dpkg-reconfigure locales</programlisting>.
      </para>
    </section>

    <section>
      <title>News from Debian Med Blend</title>
      <para>
	As in every release new packages have been added in the fields
	of medicine and life sciences. The new package <systemitem
	role="package">shiny-server</systemitem> might be worth a
	particular mention, since it simplifies scientific web
	applications using <literal>R</literal>. We also kept up the
	effort to provide Continuous Integration support for the
	packages maintained by the Debian Med team.
      </para>
      <para>
	The Debian Med team is always interested in feedback from
	users, especially in the form of requests for packaging of
	not-yet-packaged free software, or for backports from new
	packages or higher versions in testing.
      </para>
      <para>
	To install packages maintained by the Debian Med team, install
	the metapackages named
	<literal>med-<replaceable>*</replaceable></literal>, which are
	at version 3.8.x for Debian bookworm. Feel free to visit the
	<ulink url="https://blends.debian.org/med/tasks">Debian Med
	tasks pages</ulink> to see the full range of biological and
	medical software available in Debian.
      </para>
    </section>

    <section id="sbarm64" arch="arm64">
      <title>Secure Boot on ARM64</title>

      <para>
        Support for Secure Boot on ARM64 has been reintroduced in &releasename;.
        Users of UEFI-capable ARM64 hardware can boot with Secure Boot mode
        enabled and take full advantage of the security feature. Ensure that the
        packages <literal>grub-efi-arm64-signed</literal> and
        <literal>shim-signed</literal> are installed, enable Secure Boot in the
        firmware interface of your device and reboot to use your system with Secure
        Boot enabled.
      </para>

      <para>
        The <ulink url="https://wiki.debian.org/SecureBoot">Wiki</ulink> has more
        information on how to use and debug Secure Boot.
      </para>
    </section>

    <section id="dummy" condition="fixme">
      <title>Something</title>
      <para>
	Text
      </para>
    </section>

  </section>
</chapter>
